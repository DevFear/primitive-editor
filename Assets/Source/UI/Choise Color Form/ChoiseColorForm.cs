using System;
using System.Collections.Generic;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Figures;

namespace UI
{
    /// <summary>
    /// ����� ������ ����� ������.
    /// </summary>
    public class ChoiseColorForm : MonoBehaviour
    {
        ChoiseColorContainer[] _containers;

        private void Awake()
        {
            _containers = gameObject.GetComponentsInChildren<ChoiseColorContainer>(true);
        }

        public async UniTask<Color> GetColor()
        {
            gameObject.SetActive(true);
            Color? result = null;

            foreach (var container in _containers) container.IsClicked = false;

            while (true)
            {
                await UniTask.Yield();

                foreach (var container in _containers)
                {
                    if (container.IsClicked)
                    {
                        result = container.Color;
                        break;
                    }
                }

                if (result != null) break;
            }


            gameObject.SetActive(false);
            return result.Value;
        }
    }
}
