using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using Figures;


namespace UI
{
    public class CapsuleConfigurationForm : ConfigurationForm
    {
        [SerializeField] InputField _inputHeight;
        [SerializeField] InputField _inputRadius;
        [SerializeField] InputField _inputSmoothing;

        protected override IFigureSettings CreateSettings()
            => new CapsuleSettings()
            {
                Height =  float.Parse(_inputHeight.text, CultureInfo.InvariantCulture.NumberFormat),
                Radius = float.Parse(_inputRadius.text, CultureInfo.InvariantCulture.NumberFormat),
                SegmentCount = int.Parse(_inputSmoothing.text),
            };
    }
}