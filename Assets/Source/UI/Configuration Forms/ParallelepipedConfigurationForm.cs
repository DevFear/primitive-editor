using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using Figures;

namespace UI
{
    public class ParallelepipedConfigurationForm : ConfigurationForm
    {
        [SerializeField] InputField _inputLength;
        [SerializeField] InputField _inputWidth;
        [SerializeField] InputField _inputHeight;

        protected override IFigureSettings CreateSettings()
            => new ParallelepipedSettings()
            {
                Length = float.Parse(_inputLength.text, CultureInfo.InvariantCulture.NumberFormat),
                Width = float.Parse(_inputWidth.text, CultureInfo.InvariantCulture.NumberFormat),
                Height = float.Parse(_inputHeight.text, CultureInfo.InvariantCulture.NumberFormat),
            };
    }
}