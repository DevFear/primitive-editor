using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using Figures;

namespace UI
{
    public class SphereConfigurarionForm : ConfigurationForm
    {
        [SerializeField] InputField _inputRadius;
        [SerializeField] InputField _inputSmoothing;

        protected override IFigureSettings CreateSettings()
            => new SphereSettings()
            {
                Radius = float.Parse(_inputRadius.text, CultureInfo.InvariantCulture.NumberFormat),
                SegmentCount = int.Parse(_inputSmoothing.text),
            };
    }
}