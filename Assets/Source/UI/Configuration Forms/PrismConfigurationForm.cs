using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using Figures;

namespace UI
{
    public class PrismConfigurationForm : ConfigurationForm
    {
        [SerializeField] InputField _inputHeight;
        [SerializeField] InputField _inputRadius;
        [SerializeField] InputField _inputCountEdge;

        protected override IFigureSettings CreateSettings()
            => new PrismSettings()
            {
                Height = float.Parse(_inputHeight.text, CultureInfo.InvariantCulture.NumberFormat),
                Radius = float.Parse(_inputRadius.text, CultureInfo.InvariantCulture.NumberFormat),
                CountEdge = int.Parse(_inputCountEdge.text),
            };
    }
}