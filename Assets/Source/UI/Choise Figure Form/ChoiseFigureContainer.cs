using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Button))]
    public class ChoiseFigureContainer : MonoBehaviour
    {
        [SerializeField] private Type typeFigure;

        private void Awake()
        {
            var button = gameObject.GetComponent<Button>();
            button.onClick.AddListener(() => IsClicked = true);
        }

        public bool IsClicked { get; set; }
        public Type TypeFigure => typeFigure;
        

        public enum Type
        {
            Parallelepiped,
            Sphere,
            Prism,
            Capsule
        }
    }
}