using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using CameraBehaviour;
using Figures;

namespace Editing
{
    /// <summary>
    /// �������� �����, ���������� �� �������������� ������ � ���������.
    /// </summary>
    public class EditorController
    {
        ISubsystemController _flightController;
        ISubsystemController<Vector3> _figureCreationController;

        FigureDetector _detector;
        Figure _selectedFigure;

        CameraController _cameraController;

        Button _figureCreationButton;

        public EditorController(Figure.Factory factory, FigureDetector detector,
            CameraController cameraController, Button figureCreationButton)
        {
            _detector = detector;
            _figureCreationButton = figureCreationButton;
            _cameraController = cameraController;

            _flightController = new CameraFlightController(cameraController);
            _figureCreationController = new FigureCreationController(factory);
        }

        /// <summary>
        /// �������� ���������� ������ EditorController.
        /// </summary>
        public async void TransferControl()
        {
            var isFigureCreationRequest = false;
            _figureCreationButton.onClick.AddListener(() => { isFigureCreationRequest = true; });

            while (true)
            {
                await UniTask.Yield();

                if (Input.GetKeyDown(KeyCode.Mouse1)) await _flightController.TransferControl();
                else if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    var figure = _detector.Detect(Input.mousePosition);
                    if (figure != null)
                    {
                        _selectedFigure?.Unselect();
                        _selectedFigure = figure;
                        _selectedFigure.Select();
                    }
                    else
                    {
                        _selectedFigure?.Unselect();
                        _selectedFigure = null;
                    }
                }
                else if (Input.GetKeyDown(KeyCode.Delete))
                {
                    _selectedFigure?.Destroy();
                    _selectedFigure = null;
                }
                else if (isFigureCreationRequest)
                {;
                    await _figureCreationController.TransferControl(_cameraController.Position
                        + _cameraController.Forward * 3f);
                    isFigureCreationRequest = false;
                }
            }
        }
    }

    static class CursorUtility
    {
        [DllImport("user32.dll")]
        public static extern bool SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(out MousePosition position);

        [StructLayout(LayoutKind.Sequential)]
        public struct MousePosition
        {
            public int x;
            public int y;
        }
    }
}
