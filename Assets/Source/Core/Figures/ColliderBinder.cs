using System;
using System.Collections.Generic;
using UnityEngine;

namespace Figures
{
    /// <summary>
    /// ���������� ��� ������ � ����������� � ������ ������� ���� ���������.
    /// </summary>
    public static class ColliderBinder
    {
        private static Dictionary<Type, Action<IFigureSettings, Figure>> MethodsBinding
           = new Dictionary<Type, Action<IFigureSettings, Figure>>()
           {
               [typeof(ParallelepipedSettings)] = 
                   (settings, figure) => BindBoxCollider((ParallelepipedSettings)settings, figure),
               [typeof(SphereSettings)] =
                   (settings, figure) => BindSphereCollider((SphereSettings)settings, figure),
               [typeof(PrismSettings)] =
                   (settings, figure) => BindPrismCollider((PrismSettings)settings, figure),
               [typeof(CapsuleSettings)] =
                   (settings, figure) => BindCapsuleCollider((CapsuleSettings)settings, figure),
           };

        public static void BindCollider(IFigureSettings settings, Figure figure)
            => MethodsBinding[settings.GetType()](settings, figure);

        private static void BindBoxCollider(ParallelepipedSettings settings, Figure figure)
        {
            var collider = figure.gameObject.AddComponent<BoxCollider>();
            collider.size = new Vector3(settings.Width, settings.Height, settings.Length);
        }

        private static void BindSphereCollider(SphereSettings settings, Figure figure)
        {
            var collider = figure.gameObject.AddComponent<SphereCollider>();
            collider.radius = settings.Radius;
        }

        private static void BindPrismCollider(PrismSettings settings, Figure figure)
        {
            var collider = figure.gameObject.AddComponent<MeshCollider>();
            collider.convex = true;
        }

        private static void BindCapsuleCollider(CapsuleSettings settings, Figure figure)
        {
            var collider = figure.gameObject.AddComponent<CapsuleCollider>();
            collider.height = settings.Height;
            collider.radius = settings.Radius;
        }
    }
}