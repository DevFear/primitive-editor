using System;
using System.Collections.Generic;
using UnityEngine;

namespace Figures
{

    /// <summary>
    /// API ��� ������ � �������.
    /// </summary>
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class Figure : MonoBehaviour
    {
        private MeshFilter _filter;
        private MeshRenderer _renderer;
        private Color color;

        public void SetColor(Color color)
            => _renderer.material.color = this.color = color;

        public void Select()
            => _renderer.material.color = new Color(color.r, color.g, color.b, 0.5f);
        public void Unselect()
            => _renderer.material.color = new Color(color.r, color.g, color.b, 1f);

        public void Destroy()
            => GameObject.Destroy(gameObject);


        /// <summary>
        /// ��������� ������ ��� ����������� �������� ������.
        /// </summary>
        public class Factory
        {
            Figure _prefab;

            public Factory(Figure prefab) => _prefab = prefab;

            public Figure Create(IFigureSettings settings, Vector3 position, Color color)
            {
                var figure = GameObject.Instantiate<Figure>(_prefab, position, Quaternion.identity);

                figure._filter = figure.GetComponent<MeshFilter>();
                figure._renderer = figure.GetComponent<MeshRenderer>();

                var mesh = MeshCreator.Create(settings);
                figure._filter.mesh = mesh;

                figure.SetColor(color);

                ColliderBinder.BindCollider(settings, figure);

                return figure;
            }
        }
    }
}