using System;
using System.Threading;
using UnityEngine;
using Cysharp.Threading.Tasks;

namespace CameraBehaviour
{
    /// <summary>
    /// ������� ������ ����������� �� �����.
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class CameraController : MonoBehaviour
    {
        [SerializeField] float speed = 10f;
        [SerializeField] float sensitivity = 5f;

        CancellationTokenSource _sourceFlightMode;
        Camera _camera;

        private void Awake()
        {
            _camera = gameObject.GetComponent<Camera>();
        }

        public Camera Camera => _camera;
        public Vector3 Position => transform.position;
        public Vector3 Forward => transform.forward;

        public async void EnableFlightMode()
        {
            _sourceFlightMode?.Cancel();
            _sourceFlightMode?.Dispose();
            _sourceFlightMode = new CancellationTokenSource();

            var token = _sourceFlightMode;

            while (!token.IsCancellationRequested)
            {
                await UniTask.Yield();
                if (this == null) return;
                UpdateFlight();
            }
        }
        public void DisableFlightMode()
        {
            _sourceFlightMode?.Cancel();
            _sourceFlightMode?.Dispose();
            _sourceFlightMode = null;
        }
        private void UpdateFlight()
        {
            var direction = GetDirection(transform.forward, transform.right);
            transform.position += direction * speed * Time.deltaTime;

            transform.eulerAngles += GetRotation() * sensitivity;
        }


        private Vector3 GetDirection(Vector3 forward, Vector3 right)
            => forward * Input.GetAxis("Vertical") + right * Input.GetAxis("Horizontal");
        private Vector3 GetRotation()
            => new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);

    }
}